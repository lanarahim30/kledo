<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ChangeSettingTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRequiredFieldsForChangeSetting()
    {
        $this->json('PATCH','api/settings',['Accept' => 'application/json'])
                ->assertStatus(400)
                ->assertJson([
                    'success' => false,
                    'message' => 'Validation errors',
                    'errors' => [
                        'key' => [
                            'key tidak boleh kosong'
                        ],
                        'value' => [
                            'value tidak boleh kosong'
                        ]
                    ]
                ]);

        // $response->assertStatus(200);
    }

    public function testErrorInput()
    {
        $setting = [
            'key' => 'ajdakjdald',
            'value' => 10
        ];

        $this->json('PATCH', 'api/settings', $setting, ['Accept' => 'application/json'])
            ->assertStatus(400)
            ->assertJson([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => [
                    'key' => [
                        "hanya bisa diisi berdasarkan code references",
                        "hanya bisa kriteria overtime_method"
                    ],
                    'value' => [
                        "hanya bisa diisi berdasarkan id references"
                    ]
                ]
            ]);
    }

    public function testIfIdNotOverMethod()
    {
        $setting = [
            'key' => 'overtime_method',
            'value' => 10
        ];

        $this->json('PATCH', 'api/settings', $setting, ['Accept' => 'application/json'])
            ->assertStatus(400)
            ->assertJson([
                'success' => false,
                'message' => 'Validation errors',
                'errors' => [
                    'value' => [
                        "hanya bisa diisi berdasarkan id references"
                    ]
                ]
            ]);
    }

    public function testSucessfulInput()
    {
        $setting = [
            'key' => 'overtime_method',
            'value' => 2
        ];

        $this->json('PATCH', 'api/settings', $setting, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'status',
                'message',
                'data'
            ]);
    }
}
