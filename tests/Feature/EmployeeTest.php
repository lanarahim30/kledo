<?php

namespace Tests\Feature;

// use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testRequiredFieldsInput()
    {
        $this->json('POST','api/employees',['Accept' => 'application/json'])
                ->assertStatus(400);

    }

    public function testUniqueName()
    {

        $employee = [
            'name' => 'kledo',
            'salary' => 1000000
        ];

        $this->json('POST', 'api/employees', $employee, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }

    public function testangeSalaryInput()
    {
        $faker = \Faker\Factory::create();
        $employee = [
            'name' => $faker->firstname(),
            'salary' => 1000000
        ];

        $this->json('POST', 'api/employees', $employee, ['Accept' => 'application/json'])
            ->assertStatus(400);

    }

    public function testSuccessInput()
    {
        $faker = \Faker\Factory::create();

        $employee = [
            'name' => $faker->firstname(),
            'salary' => 5000000
        ];

        $this->json('POST', 'api/employees', $employee, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                'status',
                'message',
                'data' => [
                    'name',
                    'salary',
                    'updated_at',
                    'created_at',
                    'id'
                ]
            ]);
    }
}
