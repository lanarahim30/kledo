<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OvertTimeTest extends TestCase
{


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testRequiredFieldsOvertime()
    {
        $this->json('POST','api/overtimes',['Accept' => 'application/json'])
                ->assertStatus(400)
                ->assertJson([
                    'success' => false,
                    'message' => 'Validation errors',
                    'errors' => [
                        'employee_id' => [
                            'employee_id tidak boleh kosong'
                        ],
                        'date' => [
                            'tanggal tidak boleh kosong'
                        ],
                        'time_started' => [
                            'tidak boleh kosong'
                        ],
                        'time_ended' => [
                            'tidak boleh kosong'
                        ]
                    ]
                ]);
    }

    public function testIfEmployeeIdNotExists()
    {
        $overtimes = [
            'employee_id' => 100000000,
            'date' => "2022-11-26",
            'time_started' => "19:00",
            'time_ended' => "21:00",
        ];

        $this->json('POST','api/overtimes',$overtimes,['Accept' => 'application/json'])
                ->assertStatus(400)
                ->assertJson([
                    'success' => false,
                    'message' => 'Validation errors',
                    'errors' => [
                        'employee_id' => [
                            'employee_id tidak ada'
                        ]
                    ]
                ]);
    }

    public function testIfTimeEndedNotHigherThenTimeStarted()
    {
        $overtimes = [
            'employee_id' => 1,
            'date' => "2022-11-27",
            'time_started' => "22:00",
            'time_ended' => "21:00",
        ];

        $this->json('POST','api/overtimes',$overtimes,['Accept' => 'application/json'])
                ->assertStatus(400)
                ->assertJson([
                    'success' => false,
                    'message' => 'Validation errors',
                    'errors' => [
                        'time_ended' => [
                            'tidak boleh lebih dari time_started'
                        ]
                    ]
                ]);
    }

    public function testDateUniqueOvertime()
    {
        $overtimes = [
            'employee_id' => 1,
            'date' => "2022-11-23",
            'time_started' => "18:00",
            'time_ended' => "21:00",
        ];

        $this->json('POST','api/overtimes',$overtimes,['Accept' => 'application/json'])
                ->assertStatus(400)
                ->assertJson([
                    'status' => 400,
                    'errors' => [
                        'error' => 'anda sudah lembur pada tanggal ini'
                    ],
                    'message' => 'Validation Errors',
                ]);
    }

    public function testSuccessInputOvertime()
    {
        $faker = \Faker\Factory::create();

        $overtimes = [
            'employee_id' => 1,
            'date' =>  $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now')->format('Y-m-d'),
            'time_started' => "20:00",
            'time_ended' => "23:00",
        ];

        $this->json('POST','api/overtimes',$overtimes,['Accept' => 'application/json'])
                ->assertStatus(201);
    }

    public function testDateValidationCalculate()
    {
        $overtimes = [
            'month' => '20-09-2020',
        ];

        $this->json('GET','api/overtime-pays/calculate',$overtimes,['Accept' => 'application/json'])
                ->assertStatus(400)
                ->assertJson([
                    'success' => false,
                    'message' => 'Validation errors',
                    'errors' => [
                        'month' => [
                            'The month does not match the format Y-m.'
                        ]
                    ]
                ]);
    }

    public function tesGetCalculateOvertime()
    {
        $overtimes = [
            'month' => '2020-11',
        ];

        $this->json('GET','api/overtime-pays/calculate',$overtimes,['Accept' => 'application/json'])
                ->assertStatus(200);

    }
}
