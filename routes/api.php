<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    EmployeeController,
    SettingController,
    OvertimeController
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('employees',[EmployeeController::class,'storeEmployee']);
Route::post('overtimes',[OvertimeController::class,'createOvertime']);
Route::patch('settings',[SettingController::class,'changeSetting']);

Route::prefix('overtime-pays')->group(function(){
    Route::get('calculate',[OvertimeController::class,'overtimes']);
});

