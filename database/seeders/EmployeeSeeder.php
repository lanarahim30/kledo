<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\{Employee,Overtime};

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $employee = Employee::create(
            [
                'name' => 'kledo',
                'salary' => 7000000
            ]
            );

        $overtime = Overtime::create([
            'employee_id' => $employee->id,
            'date' => '2022-11-23',
            'time_started' => '18:00',
            'time_ended' => '23:00'
        ]);
    }
}
