<?php
namespace App\Repository\Service;
use App\Models\Overtime;
use Illuminate\Support\Facades\DB;

class OvertimeService {

    protected $model;

    public function __construct(Overtime $model)
    {
        $this->model = $model;
    }

    public function checkOvertime($date,$id)
    {
        return $this->model->whereDate('date',$date)->where('employee_id',$id)->first();
    }

    public function createOvertime(array $data)
    {
        return $this->model->create($data);
    }

    public function overtimes($request,$expression)
    {
        $overtimes = array();
        $outputs = array();

        $exp = ['salary','overtime_duration_total'];
        $replaceExP = ['$salary','$overtime_duration_total'];

        $replace = str_replace($exp,$replaceExP,$expression);

        $query = $this->model->query();
        // dd($request);
        if($request->month)
        {
            $query = $query->where(DB::raw("(DATE_FORMAT(date,'%Y-%m'))"),$request->month);
        }
        $query = $query->select('overtimes.*',DB::raw('FLOOR(time_to_sec(timediff(time_ended,time_started)) / 3600) as overtime_duration'));
        $results = $query->get();

        foreach($results as $key=>$result)
        {
            $overtimes[$result->employee_id][$key] = $result;
        }

        $employees = DB::table('employees')->get();
        $overtimeDuration = 0;

        foreach($employees as $key=>$employee)
        {

            $overtime = $overtimes[$employee->id] ?? null;

            $overtimeDuration = ($overtime !== null ? array_sum(array_map(fn ($item) => $item['overtime_duration'], $overtime)) : 0);

            $salary = $employee->salary;
            $overtime_duration_total = $overtimeDuration;

            $outputs[$key]['id'] = $employee->id;
            $outputs[$key]['name'] = $employee->name;
            $outputs[$key]['salary'] = $salary;
            $outputs[$key]['overtimes'] = $overtime;
            $outputs[$key]['overtime_duration_total'] = $overtimeDuration;
            $outputs[$key]['amount'] = round(eval('return '.$replace.';'));
        }

        return $outputs;
    }
}
