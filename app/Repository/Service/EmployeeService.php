<?php
namespace App\Repository\Service;
use App\Repository\EmployeeInterface;
use App\Models\Employee;

class EmployeeService implements EmployeeInterface {

    protected $model;

    public function __construct(Employee $model)
    {
        $this->model = $model;
    }

    public function addEmployee(array $data)
    {
        return $this->model->create($data);
    }

    public function getOvertime()
    {
        return $this->model->with('overtimes')->get();
    }
}
