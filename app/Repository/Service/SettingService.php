<?php
namespace App\Repository\Service;
use App\Models\Setting;

class SettingService {

    protected $model;

    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    public function getSetting()
    {
        return $this->model->with('reference')->first();
    }

    public function changeSetting(array $data,$id)
    {
        $setting = $this->model->first();

        return $setting->update($data);
    }
}
