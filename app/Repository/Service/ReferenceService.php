<?php
namespace App\Repository\Service;
use App\Models\Reference;

class ReferenceService {

    protected $model;

    public function __construct(Reference $model)
    {
        $this->model = $model;
    }

    public function findById($id)
    {
        return $this->model->find($id);
    }
}
