<?php
namespace App\Repository\Eloquent;
use App\Repository\Service\EmployeeService;

class EmployeeRepository {

    protected $service;

    public function __construct(EmployeeService $service)
    {
        $this->service = $service;
    }

    public function createEmployee($request)
    {
        try {

            $attr['name'] = $request->name;
            $attr['salary'] = $request->salary;

            $employee = $this->service->addEmployee($attr);

            if($employee)
            {
                $data = [
                    'status' => 201,
                    'message' => 'employee created',
                    'data' => $employee
                ];
            }else{
                $data = [
                    'status' => 400,
                    'message' => 'employee failed created',
                    'data' => null
                ];
            }
        } catch (\Throwable $th) {
            //throw $th;

            $data = [
                'status' => 400,
                'message' => $th->getMessage()
            ];
        }

        return $data;

    }

    public function getOvertimes()
    {
        return $this->service->getOvertime();
    }
}
