<?php
namespace App\Repository\Eloquent;
use App\Repository\Service\{
    OvertimeService,
    SettingService
};

class OvertimeRepository {

    protected $service;
    protected $setting;

    public function __construct(
        OvertimeService $service,
        SettingService $setting
    )
    {
        $this->service = $service;
        $this->setting = $setting;
    }

    public function addOvertime($request)
    {
        try {
            $employee_id = $request->employee_id;

            $attr['employee_id'] = $employee_id;
            $attr['date'] = $request->date;
            $attr['time_started'] = $request->time_started;
            $attr['time_ended'] = $request->time_ended;

            $checkOvertime = $this->service->checkOvertime($request->date,$employee_id);

            if(!$checkOvertime)
            {
                $addOvertime = $this->service->createOvertime($attr);

                if($addOvertime)
                {
                    $data = [
                        'status' => 201,
                        'message' => 'overtime created',
                        'data' => $addOvertime
                    ];
                }else{
                    $data = [
                        'status' => 400,
                        'message' => 'overtime failed created',
                        'data' => null
                    ];
                }
            }else{
                $data = [
                    'status' => 400,
                    'message' => 'anda sudah lembur pada tanggal ini',
                    'data' => null
                ];
            }

            return $data;
        } catch (\Throwable $th) {

            $data = [
                'status' => 400,
                'message' => $th->getMessage()
            ];
        }
    }

    public function overtimes($request)
    {
        $setting = $this->setting->getSetting();

        return $this->service->overtimes($request,$setting->reference->expression);
    }
}
