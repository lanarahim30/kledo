<?php
namespace App\Repository\Eloquent;
use App\Repository\Service\{
    SettingService,
    ReferenceService
};

class SettingRepository {

    protected $service;
    protected $reference;

    public function __construct(
        SettingService $service,
        ReferenceService $reference
    )
    {
        $this->service = $service;
        $this->reference = $reference;
    }

    public function changeSetting($request)
    {
        try {
            $id = $request->value;

            $attr['key'] = $request->key;
            $attr['value'] = $id;

            $reference = $this->reference->findById($id);

            if($reference && $reference->code == 'overtime_method')
            {
                $setting = $this->service->changeSetting($attr,$id);

                if($setting)
                {
                    $data = [
                        'status' => 200,
                        'message' => 'setting changed',
                        'data' => $setting
                    ];
                }else{
                    $data = [
                        'status' => 400,
                        'message' => 'setting failed changes',
                        'data' => null
                    ];
                }
            }else{
                $data = [
                    'status' => 400,
                    'message' => 'this id not overtime_method',
                    'data' => null
                ];
            }
        } catch (\Throwable $th) {
            //throw $th;

            $data = [
                'status' => 400,
                'message' => $th->getMessage()
            ];
        }

        return $data;
    }
}
