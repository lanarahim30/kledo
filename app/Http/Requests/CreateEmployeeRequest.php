<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CreateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) : JsonResponse
    {
        throw new HttpResponseException(response()->json([

            'success'   => false,

            'message'   => 'Validation errors',

            'errors'      => $validator->errors()

        ],400));
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name' => 'required|min:2|unique:employees,name',
            'salary' => 'required|numeric|between:2000000,10000000'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'nama harus diisi',
            'name.min' => 'nama minimal 2 karakter',
            'name.unique' => 'nama sudah terdaftar',
            'salary.required' => 'gaji harus diisi',
            'salary.numeric' => 'gaji harus berupa number',
            'salary.between' => 'gaji minimal 2000000 dan maksimal 10000000'
        ];
    }
}
