<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) : JsonResponse
    {
        throw new HttpResponseException(response()->json([

            'success'   => false,

            'message'   => 'Validation errors',

            'errors'      => $validator->errors()

        ],400));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'key' => 'required|exists:references,code|in:overtime_method',
            'value' => 'required|exists:references,id'
        ];
    }

    public function messages()
    {
        return [
            'key.required' => 'key tidak boleh kosong',
            'key.exists' => 'hanya bisa diisi berdasarkan code references',
            'value.required' => 'value tidak boleh kosong',
            'value.exists' => 'hanya bisa diisi berdasarkan id references',
            'key.in' => 'hanya bisa kriteria overtime_method'
        ];
    }
}
