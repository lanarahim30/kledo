<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class OvertimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function failedValidation(Validator $validator) : JsonResponse
    {
        throw new HttpResponseException(response()->json([

            'success'   => false,

            'message'   => 'Validation errors',

            'errors'      => $validator->errors()

        ],400));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'employee_id' => 'required|exists:employees,id',
            'date' => 'required|date|date_format:Y-m-d',
            'time_started' => 'required|date_format:H:i',
            'time_ended' => 'required|date_format:H:i|after_or_equal:time_started'
        ];
    }

    public function messages()
    {
        return [
            'employee_id.required' => 'employee_id tidak boleh kosong',
            'employee_id.exists' => 'employee_id tidak ada',
            'date.required' => 'tanggal tidak boleh kosong',
            'date.date' => 'tanggal salah',
            'date.date_format' => 'format tanggal salah',
            'time_started.required' => 'tidak boleh kosong',
            'time_started.date_format' => 'format salah',
            'time_ended.required' => 'tidak boleh kosong',
            'time_ended.date_format' => 'format salah',
            'time_ended.after_or_equal' => 'tidak boleh lebih dari time_started'
        ];
    }
}
