<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiHelpers;
use App\Repository\Eloquent\SettingRepository;
use App\Http\Requests\{
    SettingRequest
};


class SettingController extends Controller
{
    //
    use ApiHelpers;

    protected $settingRepo;

    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepo = $settingRepo;
    }

    /**
     * @OA\Patch(
     *     path="/api/settings",
     *     tags={"settings"},
     *     summary="Returns a data settings",
     *     description="update data settings",
     *     operationId="settings",
     *     @OA\Parameter(
     *          name="key",
     *          description="key",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="value",
     *          description="value",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="setting changed"
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="validation errors"
     *     ),
     * )
     */
    public function changeSetting(SettingRequest $request)
    {
        $setting = $this->settingRepo->changeSetting($request);

        if($setting['status'] == 400)
        {
            return $this->onError($setting['status'],['error' => $setting['message']]);
        }else{
            return $this->onSuccess($setting['data'],$setting['message'],$setting['status']);
        }
    }
}
