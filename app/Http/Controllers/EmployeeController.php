<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiHelpers;
use App\Repository\Eloquent\EmployeeRepository;
use App\Http\Requests\{
    CreateEmployeeRequest
};

class EmployeeController extends Controller
{
    use ApiHelpers;

    protected $employeeRepo;

    public function __construct(EmployeeRepository $employeeRepo)
    {
        $this->employeeRepo = $employeeRepo;
    }

    /**
     * @OA\Post(
     *     path="/api/employees",
     *     tags={"employees"},
     *     summary="Returns a data employees",
     *     description="Post data employees",
     *     operationId="employees",
     *     @OA\Parameter(
     *          name="name",
     *          description="name",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="salary",
     *          description="salary",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="employee created"
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="validation errors"
     *     ),
     * )
     */
    public function storeEmployee(CreateEmployeeRequest $request)
    {
        $employee = $this->employeeRepo->createEmployee($request);

        if($employee['status'] == 400)
        {
            return $this->onError($employee['status'],['error' => $employee['message']]);
        }else{
            return $this->onSuccess($employee['data'],$employee['message'],$employee['status']);
        }
    }

}
