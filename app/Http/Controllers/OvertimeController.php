<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\ApiHelpers;
use App\Repository\Eloquent\OvertimeRepository;
use App\Http\Requests\{
    OvertimeRequest,
    OvertimePayRequest
};

class OvertimeController extends Controller
{
    //
    use ApiHelpers;

    protected $overtimeRepo;

    public function __construct(OvertimeRepository $overtimeRepo)
    {
        $this->overtimeRepo = $overtimeRepo;
    }

    /**
     * @OA\Post(
     *     path="/api/overtimes",
     *     tags={"overtimes"},
     *     summary="Returns a data overtimes",
     *     description="Post data overtimes",
     *     operationId="overtimes",
     *     @OA\Parameter(
     *          name="employee_id",
     *          description="employee_id",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="date",
     *          description="date",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="time_started",
     *          description="time_started",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *      @OA\Parameter(
     *          name="time_ended",
     *          description="time_ended",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="overtime created"
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="validation errors"
     *     ),
     * )
     */
    public function createOvertime(OvertimeRequest $request)
    {
        $overtime = $this->overtimeRepo->addOvertime($request);

        if($overtime['status'] == 400)
        {
            return $this->onError($overtime['status'],['error' => $overtime['message']]);
        }else{
            return $this->onSuccess($overtime['data'],$overtime['message'],$overtime['status']);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/overtime-pays/calculate",
     *     tags={"overtime-pays/calculate"},
     *     summary="Returns calculate overtime pay",
     *     description="Returns calculate overtime pay",
     *     operationId="calculate",
     *     @OA\Parameter(
     *          name="month",
     *          description="month",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="successful calculate"
     *     )
     * )
     */
    public function overtimes(OvertimePayRequest $request)
    {
        $overtimes = $this->overtimeRepo->overtimes($request);

        return $this->onSuccess($overtimes,'overtimes',200);
    }
}
